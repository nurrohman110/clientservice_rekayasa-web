@extends('template')

@section('content')
<h3>Tambah Customer</h3>
    <div class="row">
        <form method="POST" action="{{ url('/customer/insert') }}">
            @csrf
            <div class="col-md-8">
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Nama Customer</label>
                    <div class="col-sm-10">
                        <input type="text" required name="customerName" placeholder="Nama Customer" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Alamat Customer</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" name="customerCity" placeholder="Alamat Customer">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Nomor Telepon Customer</label>
                    <div class="col-sm-10">
                        <input type="number" required min="0" class="form-control" name="customerPhone" placeholder="Nomor Telepon Customer">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
@endsection