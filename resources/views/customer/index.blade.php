@extends('template')

@section('content')
    <div class="row">
        <a href="{{ url('customer/tambah') }}" class="btn btn-primary btn-sm">Tambah</a>
        <div class="table-responsive">
            <table class="table table-dark table-striped table-bordered">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>Nama Customer</th>
                        <th>Alamat Customer</th>
                        <th>Nomor Telepon Customer</th>
                        <th>Dibuat pada tanggal</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($customer as $c)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $c->customerName }}</td>
                            <td>{{ $c->customerCity }}</td>
                            <td>{{ $c->customerPhone }}</td>
                            <td>{{ $c->created_at }}</td>
                            <td>
                                <a href="{{ url('customer/edit/') }}/{{ $c->customerId }}" class="btn btn-primary btn-sm">Edit</a>
                                <a href="{{ url('customer/delete') }}/{{ $c->customerId }}" class="btn btn-danger btn-sm">Hapus</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection