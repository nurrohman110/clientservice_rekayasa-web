<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class CustomerController extends Controller
{

    // sesuaikan dengan op local
    private $api = 'x.x.x.x/webservice_rekayasa_web/api/';

    public function index(){
        $client = new Client();
        $res = $client->get($this->api.'customerGet');
        $jsonDecode = json_decode($res->getBody());
        $data = [
            'customer' => $jsonDecode->customer,
        ];
        return view('customer.index',$data);
    }

    public function insert(Request $request){
        $client = new Client();
        $url = $this->api.'customerPost';
        $response = $client->request('POST', $url, [
            'form_params' => [
               'customerName' => $request->customerName,
                'customerCity' => $request->customerCity,
                'customerPhone' => $request->customerPhone,
            ]
        ]);
        return redirect('/customer');
    }

    public function edit($id){
        $client = new Client();
        $res = $client->get($this->api.'customerGet?id='.$id);
        $jsonDecode = json_decode($res->getBody());
        $data = [
            'customer' => $jsonDecode->customer,
        ];
        return view('customer.edit',$data);
    }

    public function update(Request $request){
        $client = new Client();
        $url = $this->api.'customerUpdate';
        $response = $client->request('POST', $url, [
            'form_params' => [
                'id' => $request->customerId,
               'customerName' => $request->customerName,
                'customerCity' => $request->customerCity,
                'customerPhone' => $request->customerPhone,
            ]
        ]);
        return redirect('/customer');
    }

    public function delete($id){
        $client = new Client();
        $url = $this->api.'customerDelete?id='.$id;
        $response = $client->delete($url);
        return redirect('/customer');
    }
}

