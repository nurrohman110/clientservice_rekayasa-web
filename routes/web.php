<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Route::get('/customer', [CustomerController::class,'index']);
Route::get('/customer/tambah',function(){
    return view('customer.tambah');
});
Route::post('/customer/insert',[CustomerController::class,'insert']);
Route::get('/customer/edit/{id}',[CustomerController::class,'edit']);
Route::post('/customer/update',[CustomerController::class,'update']);
Route::get('/customer/delete/{id}', [CustomerController::class,'delete']);